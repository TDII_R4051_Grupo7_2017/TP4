/*
 * ADS1115.h
 *
 *  Created on: 28/02/2017
 *      Author: Cesar
 */

#ifndef ADS1115_H_
#define ADS1115_H_


#define DATA_SIZE		2048

#define ADS1115_ADDR			0x48 // 1001000b
#define GENERAL_CALL_ADDR		0x00

typedef enum addr_pointer_reg {
	CONVERSION_REG = 0,
	CONFIG_REG,
	LO_THRESH_REG,
	HI_THRESH_REG
} ADDR_POINTER_REG_T;


typedef enum operating_mode {
	SINGLE_SHOT = 0x8583,
	CONTINUOUS_CONVERSION = 0x8483,
	FSR4096 = 0x8283,
	DATARATE_860= 0x82E3
} OPERATING_MODE_T;


void ADS1115_Init( void );
void ADS1115_SetConfigRegister( OPERATING_MODE_T mode );
Status ADS1115_ReadConversion( uint32_t muestras, uint16_t* dato );//aca modifique el prototipo para que me decuelva el dato
uint16_t ADS1115_GetConfigRegister( void );
uint8_t ADS1115_GeneralCall( void );

#endif /* ADS1115_H_ */
